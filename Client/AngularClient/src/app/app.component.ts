import { Component, OnInit, HostListener } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { fetch, remove } from './../core/store/post.actions';
import { Post } from './../core/model/post';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConformationDialogComponent } from './conformation-dialog/conformation-dialog.component';
import * as moment from 'moment';
import { AppState } from 'src/core/store/post.reducer';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  dialogRef: MatDialogRef<ConformationDialogComponent>;
  displayedColumns: string[] = ['Title', 'Date', 'Remove'];
  dataSource: Observable<any>;
  showProgressBar: Observable<boolean>;
  title = 'AngularClient';
  constructor(private store: Store<AppState>, private dialog: MatDialog) {
    this.dataSource = this.store.select('posts');
    this.showProgressBar = this.store.select('loading');
  }
  ngOnInit(): void {
    this.store.dispatch(fetch());
  }
  remove(id: number, event): void {
    event.stopPropagation();
    this.dialogRef = this.dialog.open(ConformationDialogComponent, {
      disableClose: false
    });
    this.dialogRef.componentInstance.confirmMessage = 'Are you sure you want to delete?';

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.store.dispatch(remove({id}));
      }
      this.dialogRef = null;
    });
  }
  openUrl(post: Post) {
    window.open(post.story_url ?? post.url, '_blank');
  }
  formatDate(date) {
    return moment(date).calendar(null, {
      sameDay: 'h:mm a',
      nextDay: '[Tomorrow]',
      nextWeek: 'MMMM DD, YYYY',
      lastDay: '[Yesterday]',
      lastWeek: 'MMMM DD',
      sameElse: 'MMMM DD, YYYY'
  });
  }
  @HostListener('window:scroll', ['$event'])
  onScroll(event) {
    const scrollPosition: number = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    const maxScrollHeight: number = document.documentElement.scrollHeight;
    const scrollPercent: number = scrollPosition / maxScrollHeight * 100;
    if (scrollPercent > 80) {
      this.store.dispatch(fetch());
    }
  }
}
