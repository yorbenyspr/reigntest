import { MatDialogModule } from '@angular/material/dialog';
import { PostService } from './../core/services/post.service';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { StoreModule } from '@ngrx/store';
import { postReducer, loadingReducer } from './../core/store/post.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PostEffects } from './../core/store/post.effects';
import {MatToolbarModule} from '@angular/material/toolbar';
import { ConformationDialogComponent } from './conformation-dialog/conformation-dialog.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { RequestInterceptor } from 'src/core/interceptors/http.interceptor';
@NgModule({
  declarations: [
    AppComponent,
    ConformationDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatDialogModule,
    MatProgressBarModule,
    StoreModule.forRoot({ posts: postReducer, loading: loadingReducer }),
    EffectsModule.forRoot([PostEffects])
  ],
  providers: [
    PostService,
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
