import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
@Component({
  selector: 'app-conformation-dialog',
  templateUrl: './conformation-dialog.component.html',
  styleUrls: ['./conformation-dialog.component.scss']
})
export class ConformationDialogComponent implements OnInit {
  confirmMessage = '';
  constructor(public dialogRef: MatDialogRef<ConformationDialogComponent>) {}

  ngOnInit(): void {
  }

}
