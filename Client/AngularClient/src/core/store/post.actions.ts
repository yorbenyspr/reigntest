import { createAction, props  } from '@ngrx/store';
import { Post } from './../model/post';
export const fetch = createAction('[POST API] Fetch');
export const fetched = createAction('[POST API] Fetched', props<{payload: Array<Post>}>());
export const remove = createAction('[POST API] Delete', props<{ id: number}>());
export const removed = createAction('[POST API] Deleted', props<{ id: number}>());
export const fetching = createAction('[POST API] Fetching Data');
export const endfetching = createAction('[POST API] End Fetching Data');
