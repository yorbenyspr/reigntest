import { createReducer, on } from '@ngrx/store';
import { fetched, removed, fetching, endfetching } from './post.actions';
import { Post } from './../model/post';
export interface AppState {
  posts: Array<Post>;
  loading: boolean;
}
export const initialState: Array<Post> = [];
export const loading = false;
const postReducers = createReducer(initialState,
  on(fetched, (state, action) => state.concat(action.payload)),
  on(removed, (state, action) => state.filter( value => value.story_id !== action.id))
);

const loadingReducers = createReducer(loading,
  on(fetching, () => true),
  on(endfetching, () => false)
  );

export function postReducer(state, action) {
  return postReducers(state, action);
}

export function loadingReducer(state, action) {
  return loadingReducers(state, action);
}

