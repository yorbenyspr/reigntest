import { fetch, remove, fetched, removed } from './post.actions';
import { PostService } from './../services/post.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError, filter } from 'rxjs/operators';
@Injectable()
export class PostEffects {
  fetchPosts = createEffect(() => this.actions.pipe(
    ofType(fetch),
    mergeMap(() => this.postService.getPosts()
      .pipe(
        filter( posts => this.filterData(posts)),
        map(posts => fetched({payload: posts})),
        catchError(() => EMPTY)
      ))
    )
  );
  removePosts = createEffect(() => this.actions.pipe(
      ofType(remove),
      mergeMap(
          action => this.postService.deletePost(action.id).pipe(
              map( () => removed({id: action.id})),
              catchError(() => EMPTY)
          )
      )
  ));
  constructor(
    private actions: Actions,
    private postService: PostService
  ) {}
  filterData(data) {
    return data.filter( value => value.story_title || value.title);
  }
}
