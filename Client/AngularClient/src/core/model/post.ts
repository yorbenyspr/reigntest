export interface Post {
    story_title: string;
    title: string;
    author: string;
    created_at: string;
    created_at_i: number;
    story_id: number;
    story_url: string;
    url: string;
}
