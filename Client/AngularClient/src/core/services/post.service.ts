import { tap } from 'rxjs/operators';
import { Observable, EMPTY } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment.prod';
import { Post } from './../model/post';
@Injectable({
  providedIn: 'root'
})
export class PostService {
  page = 0;
  fetchMore = true;
  constructor(private http: HttpClient) { }
  getPosts(): Observable<Array<Post>> {
    if (this.fetchMore) {
      const pageAux = this.page;
      this.page++;
      return this.http.get<Array<Post>>(`${environment.baseUrl}/posts?page=${pageAux}`).pipe(
        tap(
            (posts) => {
              if (posts && posts.length !== 20) {
                this.fetchMore = false;
              }
              return posts;
          }));
    } else {
      return EMPTY;
    }
  }
  deletePost(id: number) {
    return this.http.delete(`${environment.baseUrl}/post/${id}`);
  }
}
