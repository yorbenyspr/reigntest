import { finalize } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../store/post.reducer';
import { fetching, endfetching } from '../store/post.actions';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    httpRequests: Array<HttpRequest<any>> = [];
    constructor(private store: Store<AppState>) {}
    removeRequest(red): void {
        const index = this.httpRequests.indexOf(red);
        if (index >= 0 && index < this.httpRequests.length) {
            this.httpRequests.splice(index, 1);
        }
        if (this.httpRequests.length === 0) {
            this.store.dispatch(endfetching());
        }
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.httpRequests.push(req);
        this.store.dispatch(fetching());
        return next.handle(req)
        .pipe(
            finalize(() => { this.removeRequest(req);
            }));
    }
}
