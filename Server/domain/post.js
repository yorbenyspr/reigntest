const Helper = require('./../helpers/helpers');
module.exports = class Post{
    constructor(dburl,dbname,collection, dbclient){
        this.dburl = dburl;
        this.dbname = dbname;
        this.collection = collection;
        this.dbclient = dbclient;
    }
    async SavePost(post){
        const connection = await this.dbclient.connect(this.dburl,{useUnifiedTopology: true});
        const db = connection.db(this.dbname);
        const result = await db.collection(this.collection).insertOne(post);
        connection.close();
        return result;
    }
    async SavePosts(posts){
        const connection = await this.dbclient.connect(this.dburl,{useUnifiedTopology: true});
        const db = connection.db(this.dbname);
        const result = await db.collection(this.collection).bulkWrite(Helper.GetUpdateOperations(posts));
        connection.close();
        return result;
    }
    async GetPosts(page){
        const connection = await this.dbclient.connect(this.dburl,{useUnifiedTopology: true});
        const db = connection.db(this.dbname);
        const result = await db.collection(this.collection)
                                .find({ deleted: {$ne: true}})
                                .sort( { created_at_i: -1 } )
                                .skip(page*20)
                                .limit(20)
                                .toArray();
        connection.close();
        return result;
    }
    async DeletePost(id){
        const connection = await this.dbclient.connect(this.dburl,{useUnifiedTopology: true});
        const db = connection.db(this.dbname);
        const result = await db.collection(this.collection).updateOne({story_id: id}, { $set: { "deleted" : true } });
        connection.close();
        return result;
    }
}
