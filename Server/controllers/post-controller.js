module.exports = class PostController{
    constructor(postService){
        this.postService = postService;
    }
    async GetPosts(context){
        let page = context.request.query.page || 0;
        context.body = await this.postService.GetPosts(Number.parseInt(page));
    }
    async DeletePost(context){
        context.body ={success: await this.postService.DeletePost(Number.parseInt(context.params.id))};
    }
}