const PostService = require('./../services/post-service');
const Post = require('./../domain/post');
const dbclient = require('./mocks/dbclient.mock');
const httpclient = require('./mocks/httpclient.mock');
var post = new Post('someurl','somedb','somecollection',dbclient);
const service = new PostService('someurl',post,httpclient);
describe('Service Test', () => {
    it('Should test PostService.PullFromApi and dont throw any exception', async () => {
      await service.PullFromApi();
      expect(true).toBe(true);
    }),
    it('Should test PostService.GetPosts and dont throw any exception', async () => {
        let result = await service.GetPosts(0);
        expect(result.length).toBe(0);
    }),
    it('Should test PostService.DeletePost and dont throw any exception', async () => {
       let result = await service.DeletePost(5);
       expect(result).toBe(1);
    })
  })