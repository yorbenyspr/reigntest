const Post = require('./../domain/post');
const client = require('./mocks/dbclient.mock'); 

var post = new Post('someurl','somedb','somecollection',client);
describe('Domain Test', () => {
    it('Should test Pos.SavePost and dont throw any exception', async () => {
      let result = await post.SavePost();
      expect(result.ok).toBe(1);
    }),
    it('Should test Pos.SavePosts with no elements and dont throw any exception', async () => {
      let result = await post.SavePosts([]);
      expect(result.ok).toBe(1);
    }),
    it('Should test Pos.SavePosts with elements and dont throw any exception', async () => {
      let result = await post.SavePosts([{test: 'test'}]);
      expect(result.ok).toBe(1);
    }),
    it('Should test Pos.GetPosts and dont throw any exception', async () => {
      let result = await post.GetPosts([{test: 'test'}]);
      expect(result.length).toBe(0);
    }),
    it('Should test Pos.DeletePost and dont throw any exception', async () => {
      let result = await post.DeletePost(5);
      expect(result.result.ok).toBe(1);
    })
  })