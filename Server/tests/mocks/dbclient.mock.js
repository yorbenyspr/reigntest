const cursorMock = {
    sort: function(){
        return cursorMock;
    },
    skip: function(){
        return cursorMock;
    },
    limit: function(){
        return cursorMock;
    },
    toArray: async function(){
        return [];
    }
};
module.exports = {
    connect: async function(){
      return {
        close: function(){
  
        },
        db: function(db) {
          return {
            collection: function(collection){
              return {
                insertOne: async function(element){
                    return { ok : 1};
                },
                bulkWrite: async function(operations){
                    return { ok : 1};
                },
                find: function(){
                    return cursorMock;
                },
                updateOne: async function(){
                    return {result:{ok: 1}};
                }
              }
            }
          }
        }
      }
    }
  };