const PostService = require('./../services/post-service');
const Post = require('./../domain/post');
const PostController = require('./../controllers/post-controller');
const dbclient = require('./mocks/dbclient.mock');
const httpclient = require('./mocks/httpclient.mock');
var post = new Post('someurl','somedb','somecollection',dbclient);
const service = new PostService('someurl',post,httpclient);
const postController = new PostController(service);
describe('Controller Test', () => {
    it('Should test PostController.GetPosts and dont throw any exception', async () => {
      let context = {request:{query:{}}};
      await postController.GetPosts(context);
      expect(context.body.length).toBe(0);
    }),
    it('Should test PostController.DeletePost and dont throw any exception', async () => {
        let context = {params:{id:5}};
        await postController.DeletePost(context);
        expect(context.body.success).toBe(1);
      })
  })
