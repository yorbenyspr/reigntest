const Router = require("koa-router");
const di = require('./../di/dicontainer');
module.exports = function(){
    const router = new Router();
    const postController = di.container.PostController;
    router.get("/posts", postController.GetPosts.bind(postController));
    router.delete("/post/:id", postController.DeletePost.bind(postController));
    return router;
}