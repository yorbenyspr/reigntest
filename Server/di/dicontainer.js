require('dotenv').config();
const axios = require('axios');
const MongoClient = require('mongodb').MongoClient;
const DiContaier = require('bottlejs');
const di = new DiContaier();
const Post = require('./../domain/post');
const PostService = require('./../services/post-service');
const PostController = require('./../controllers/post-controller');
di.factory('Post', function() {
    return new Post(process.env.MONGO_URL,'reignposts','posts', MongoClient);
});
di.factory('PostService',function(container){
    return new PostService(process.env.API_URL,container.Post,axios);
});
di.service('PostController', PostController, 'PostService');

module.exports = di;