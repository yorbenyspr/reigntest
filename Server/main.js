const Koa = require("koa");
const cors = require('@koa/cors');
const schedule = require('node-schedule');
const router = require('./routes/postroute')();
const app = new Koa();
const di = require('./di/dicontainer');
const postService = di.container.PostService;

app.use(cors());



app.use(router.routes()).use(router.allowedMethods());

schedule.scheduleJob('0 */1 * * *', postService.PullFromApi.bind(postService));

postService.PullFromApi();
console.log('Running ;)');
app.listen(3000);