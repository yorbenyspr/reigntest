module.exports = class Helper{
    static GetUpdateOperations(posts){
        let operations = [];
        posts.forEach(element => {
            var {story_title,title,author,created_at,created_at_i,story_id,story_url,url} = element;
            operations.push({
                updateOne: {
                    filter: {'story_id': element.story_id},
                    update: {$set:{story_title,title,author,created_at,created_at_i,story_id,story_url,url}},
                    upsert: true
                }
            })
        });
        return operations;
    }
}