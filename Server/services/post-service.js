module.exports = class PostService{
    constructor(apiUrl,post,httpClient){
        this.apiUrl = apiUrl;
        this.post = post;
        this.httpClient = httpClient;
    }
    async PullFromApi(){
        let page = 0;
        let continueQuery = false;
        do{
            const {data} = await this.httpClient.get(`${this.apiUrl}&page=${page}`);
            await this.post.SavePosts(data.hits);
            page++;
            continueQuery = page < data.nbPages;
        }while(continueQuery);
    }
    async GetPosts(page){
        return await this.post.GetPosts(page);
    }
    async DeletePost(id){
        let {result} = await this.post.DeletePost(id);
        return result.ok;
    }
}