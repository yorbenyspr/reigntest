# ReignTest

Check out master branch

**Installing tools**
1.  Install latest version of Node Js
2.  Install angular cli `npm install -g @angular/cli`
3.  Install mongodb and configure it for listening on port 27017

**Configure mongodb to improve queries**
1.  In order to improve queries and avoid collection scan you must create a mongodb index in related fields (`story_id` and `created_at_i`)
2.  Run the fallowing command `db.getCollection('posts').createIndex( { story_id: 1 } )` and `db.getCollection('posts').createIndex( { created_at_i: -1 } )`

**Runing the server component**
1.  Open a new powershell and go to $root/Server
2.  run `npm i`
3.  run `node main.js`

**Runing the client component**
1.  Open a new powershell and go to $root/Client/AngularClient
2.  run `npm i`
2.  run `ng serve`
3.  Open a web browser and navigate to "http://localhost:4200/"
4.  Happy reviewing ;)

**Running in a container**
1.  Install Docker and Dorker Compose ([https://www.docker.com/get-started](https://www.docker.com/get-started))
2.  Open a new powershell and go to $root
3.  run docker-compose -f docker-compose.yml up -d --build
4.  Open a web browser and navigate to "http://$HOST_IP:80/"
5.  If any mapping ports in docker-compose.yml are used by your host you must change these ports
6.  ;)
